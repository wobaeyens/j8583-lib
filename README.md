# j8583

j8583 is a Java implementation of the ISO8583 protocol.

This repository is a **fork** of [https://bitbucket.org/chochos/j8583](https://bitbucket.org/chochos/j8583).

## How to release

To release to Maven Central, go to [branches](https://bitbucket.org/thibaudledent/j8583/branches/) and run the dedicated pipeline:

![gif](https://bitbucket.org/thibaudledent/j8583/raw/ded5f57141cf1680b5debbfe77fa84de3e8f4282/how_to_release.gif)

## Creating a Bitbucket pipeline to automate a Maven release
Here is the [link](https://thibaudledent.github.io/2019/03/01/bitbucket-pipeline/).